rm -rf /opt/ANDRAX/cfr

mkdir /opt/ANDRAX/cfr

cp -Rf cfr.jar /opt/ANDRAX/cfr

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
